App.controller('mainController', function($scope,$http,URL_API) {
    $scope.message = 'Yuk Nabung!';
    $http.get(URL_API + 'api/v1/total')
        .then(function(response){
        	$scope.total=response.data.body;
        	console.log($scope.total);
        });
});

App.controller('aboutController', function($scope) {
    $scope.message = 'Look! I am an about page.';
});

App.controller('contactController', function($scope) {
    $scope.message = 'Contact us! JK. This is just a demo.';
});

App.controller('studentController', function($scope){
    $scope.message ='student.';
    
});

App.controller('loginController', function($scope){
    $scope.message ='Login.';
    
});

App.controller('registerController', function($scope){
    $scope.message ='Register.';
    
});

App.controller('tabunganController', function($scope,$http,URL_API){
    $scope.message ='Yuk Nabung!';
    $http.get(URL_API + 'api/v1/tabungan')
        .then(function(response){
        	$scope.tabungan=response.data.body;
        	console.log($scope.tabungan);
        });
    $http.get(URL_API + 'api/v1/tabungan/list')
        .then(function(response){
        	$scope.list=response.data.body;
        	console.log($scope.list);
        });
});


