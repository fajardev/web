App.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController'
        })

        // route for the about page
        .when('/about', {
            abstract          : true,
            templateUrl : 'pages/about.html',
            controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'pages/contact.html',
            controller  : 'contactController'
        })

        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })

        .when('/register', {
            templateUrl : 'pages/register.html',
            controller  : 'registerController'
        })

        .when('/tabunganku', {
            templateUrl : 'pages/tabungan.html',
            controller  : 'tabunganController'
        });
});