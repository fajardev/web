App.controller('pemasukanController', function($scope,$http,URL_API){
    $scope.message ='Pemasukan Uang';
    $http.get(URL_API + 'api/v1/pemasukan')
        .then(function(response){
        	$scope.pemasukan=response.data.body;
        	console.log($scope.pemasukan);
        });
    $http.get(URL_API + 'api/v1/pemasukan/list')
        .then(function(response){
        	$scope.list=response.data.body;
        	console.log($scope.list);
        });
});
