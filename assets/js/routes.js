// configure our routes
    App.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'mainController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'pages/about.html',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/input', {
                templateUrl : 'pages/input.html',
                controller  : 'inputController'
            });
    });

    // create the controller and inject Angular's $scope
    App.controller('mainController', function($scope) {
        // create a message to display in our view
        $scope.message = 'Everyone come and see how good I look!';
    });

    App.controller('aboutController', function($scope) {
        $scope.message = 'Look! I am an about page.';
    });

    App.controller('contactController', function($scope) {
        $scope.message = 'Contact us! JK. This is just a demo.';
    });

    App.controller('inputController', function($scope){
        $scope.message='Input.';
    });