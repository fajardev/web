
/* form validation Create New User*/
$(document).ready (function() {
	
    $("form[name='myForm']").bootstrapValidator({
      	feedbackIcons: {
       	    valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
      	},
      
      	fields: {
           	myName: {
            	validators: {
                    stringLength: {
                        min: 2,
                        message:'Tidak valid'
                    },
             		notEmpty: {
             			message: 'Nama Tidak Boleh Kosong!!!'
             		},
             		regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Nama Hanya Boleh Mengandung Huruf.'
                    }
           		}
           	}
            
        }
    })
});

